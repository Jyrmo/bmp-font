const renderer = require('@jyrmo/renderer')

const bmp = require('../lib/index')

const Font = bmp.default

const img = new Image()
img.src = 'Consolas-72.png'

const req = new XMLHttpRequest()
let fileContent = null

let numLoaded = 0

let font = null

const xStart = -200
let x = xStart
const yBase = 120

const buildEntity = (char, idx) => {
  const glyph = font.getGlyph(char)

  x += glyph.xOffset / 2
  const y = yBase - glyph.yOffset / 2

  const transform = {
    pos: [x, y],
    scale: [glyph.sprite.width, glyph.sprite.height],
    rot: 0
  }

  x += glyph.xAdvance

  const entity = { transform }
  new renderer.SpriteRenderable(entity, glyph.sprite)

  return entity
}

const start = () => {
  const canvas = document.getElementById('canvas')
  const shaderSpec = { builtIn: 'sprite' }
  const resolution = [800, 600]
  renderer.init({canvas, shader: shaderSpec, resolution})

  font = new Font(img, fileContent)
  
  renderer.clear()

  const str = 'Jürmo was here!'
  
  for (let idx = 0; idx < str.length; idx++) {
    const entity = buildEntity(str[idx], idx)
    entity.renderable.render()
  }
}

const addLoaded = () => {
  numLoaded++
  if (numLoaded === 2) {
    start()
  }
}

req.open('GET', 'Consolas-72.fnt', true)
req.setRequestHeader('Content-Type', 'text/xml')

req.onload = () => {
  const parser = new DOMParser()
  fileContent = req.responseText
  addLoaded()
}

req.send()

img.onload = () => {
  addLoaded()
}