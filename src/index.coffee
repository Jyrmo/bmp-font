fonts = {}

parser = new DOMParser()

# TODO: binary fnt files?
# TODO: specify fallbackGlyph in font

class Font
  # img is an Image
  # fnt is the string contents of a .fnt file
  constructor: (@img, fnt) ->
    @info = parser.parseFromString fnt, 'application/xml'

  # char is a string of length 1
  getGlyph: (char) ->
    code = char.charCodeAt 0
    @getGlyphByCharCode code

  getGlyphByCharCode: (code) ->
    # TODO: error if no such glyph
    charPath = "font/chars/char[@id=#{code}]"
    charInfo = @info.evaluate charPath, @info, null, XPathResult.ANY_TYPE, null
    node = charInfo.iterateNext()
    {
      sprite: {
        img: @img
        x: Number node.getAttribute 'x'
        y: Number node.getAttribute 'y'
        width: Number node.getAttribute 'width'
        height: Number node.getAttribute 'height'
      }
      xOffset: Number node.getAttribute 'xoffset'
      yOffset: Number node.getAttribute 'yoffset'
      xAdvance: Number node.getAttribute 'xadvance'
    }

export default Font


